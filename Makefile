COMPILER = nasm
LINKER = ld
FLAG = -g -felf64
TARGET = yap_lab2

$(TARGET) : main.o dict.o lib.o
	$(LINKER) $^ -o $(TARGET)

%.o : %.asm lib.inc dict.inc words.inc 
	$(COMPILER) $(FLAG) $< -o $@ 

.PHONY: clean
clean :
	rm -f *.o
	rm -f $(TARGET)
