%include "lib.inc"

%define POINTER_SIZE 8

global find_word
section .text
find_word:
    .loop:
        push    rsi                 ; сохранили адрес на следующий
                                    ; элемент
        add     rsi, POINTER_SIZE   ; переход к ключу элемента
        push    rdi
        call    string_equals
        pop     rdi
        pop     rsi                 ; возврат к адресу на следующий элемент
        cmp     rax, 1              ; проверяем, совпал ли ключ
        je      .end                ; да - выход
        ;cmp    qword[rsi], 0   
        mov     r8, qword[rsi] 
        test    r8, r8   ; если адрес равен 0, то мы достигли 
                                    ; конца словаря
        je      .not_found
        mov     rsi, r8          ; берем адрес на следующий элемнт
        jmp     .loop
    .not_found:
        xor     rsi, rsi
    .end:
        mov     rax, rsi            ; в rax помещаем адрес на найденный
                                    ; элемент или ноль, если он не найден
        ret
        