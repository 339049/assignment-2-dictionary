%include "lib.inc"
%include "dict.inc"
%include "colon.inc"
%include "words.inc"

%define BUFF_SIZE       256
%define POINTER_SIZE    8

%ifndef ELEMENT
%define ELEMENT         0
%endif

section .bss
buff: resb BUFF_SIZE
section .rodata
err_msg: db "Key not found", `\n`, 0
buff_limit_msg: db "The buffer is full", `\n`, 0


section .text
global _start
print_error:
    push    rdi
    call    string_length
    pop     rdi
    mov     rdx, rax
    mov     rsi, rdi
    mov     rax, 1
    mov     rdi, 2
    syscall
    ret

_start:
    mov rsi, BUFF_SIZE
    mov rdi, buff
    call read_word
    cmp rax, 0
    je .print_err_buff

    mov     rdi, rax
    mov     rsi, ELEMENT
    call    find_word   
    ; cmp     rax, 0
    test    rax, rax
    je      .print_err_msg

    add     rax, POINTER_SIZE  ; пропускаем адрес сл. элемента
    mov     rdi, rax        ; передаем адрес ключа
    push    rdi
    call    string_length   ; находим длину ключа
    pop     rdi
    add     rdi, rax        ; пропускаем ключ
    inc     rdi             ; пропускаем 0 (конец строки)
    call    print_string    ; печатаем элемент
    call    print_newline
    call    exit

    .print_err_buff:
        mov rdi, buff_limit_msg
        call    print_error
        call exit

    .print_err_msg:
        mov rdi, err_msg
        call    print_error
        call exit
