global exit
global print_newline
global print_string
global read_word
global string_equals
global string_length



section .text

%define EXIT_CODE 60

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_CODE
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
  xor   rcx, rcx
  .loop:
    ; cmp   byte [rdi+rcx], 0
    mov  r8b,  byte[rdi+rcx]
    test r8b, r8b

    je    .end
    inc   rcx
    jmp   .loop
  .end:
    mov rax, rcx
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
  push rdi 
  call  string_length
  pop rdi
  mov   rdx, rax
  mov   rax, 1
  mov   rsi, rdi
  mov   rdi, 1
  syscall
  ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
  mov  rdi, `\n`
  
; Принимает код символа и выводит его в stdout
print_char:
  push  rdi
  mov   rdi, 1
  mov   rdx, 1
  mov   rax, 1
  mov   rsi, rsp
  syscall
  pop   rdi
  ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
  ; cmp   rdi, 0
  test  rdi, rdi
  jge   print_uint
  push  rdi
  mov   rdi, '-'
  call  print_char
  pop   rdi
  neg   rdi
  

; Выводит беззнаковое 8-байтовое число в десятичном формате 
print_uint:
    push    rbx
    mov     r8, rsp
    dec     rsp
    mov     byte [rsp], 0
    mov     rbx, 10
    mov     rax, rdi
.loop:
    xor     rdx, rdx
    div     rbx
    add     rdx, '0'
    dec     rsp
    mov     [rsp], dl
    ; cmp     rax, 0
    test    rax, rax
    jne     .loop   
    mov     rdi, rsp
    call    print_string 
    mov     rsp, r8
    pop     rbx
    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
  .loop:
    mov   al, [rdi]
    cmp   al, [rsi]
    jne   .false
    test  al, al
    je    .true
    inc   rdi
    inc   rsi
    jmp   .loop
  .true:
    mov   rax, 1
    ret
  .false: 
    xor   rax, rax 
    ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char: 
    xor rax,rax 
    xor rdi,rdi
    mov rdx,1 
    push 0
    mov rsi,rsp 
    syscall 
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
  mov   r8, rsi 
  mov   r9, rdi 
  xor   r10, r10 
  xor   rdi, rdi 
  mov   rdx, 1 
  .skip:
    xor   rax, rax 
    mov   rsi, r9 
    syscall
    test  rax, rax
    je    .zero 
    cmp   byte[r9], ' ' 
    je    .skip  
    cmp   byte[r9], `\t`
    je    .skip
    cmp   byte[r9], `\n` 
    je    .skip
    inc   r10 
  .read:
    xor   rax, rax 
    lea   rsi, [r9+r10] 
    syscall
    mov   cl, byte[r9+r10]
    test  rax, rax
    jz    .zero 
    cmp   cl, ' ' 
    je    .zero 
    cmp   cl, `\t`
    je    .zero 
    cmp   cl, `\n` 
    je    .zero 
    cmp   r8, r10 
    jl    .of 
    inc   r10 
    jmp   .read 
  .zero:
    mov   byte[r9+r10], 0
    mov   rdx, r10 
    mov   rax, r9
    ret
  .of:
    xor   rdx, r8 
    xor   rax, rax
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
  xor   r8, r8
  xor   rax, rax
  xor   rdx, rdx
  .loop:
    cmp   byte[rdi+rdx], '0'
    jl    .exit
    cmp   byte[rdi+rdx], '9'
    jg    .exit
    mov   r8b, [rdi+rdx] 
    sub   r8b, '0' 
    imul  rax, 10
    add   rax, r8 
    inc   rdx 
    jmp   .loop
  .exit:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
  xor   rax, rax
  cmp   byte[rdi], '-'
  jne   parse_uint
  inc   rdi
  call  parse_uint
  neg   rax
  inc   rdx
  ret 


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
  xor   rcx, rcx
  .loop:
    cmp   rcx, rdx
    jge   .of
    mov   al, [rdi+rcx]
    mov   [rsi+rcx], al
    inc   rcx
    cmp   al, 0
    je    .end
    jmp   .loop
  .of:
    xor   rax, rax
    ret
  .end:
    mov   rax, rcx
    ret
