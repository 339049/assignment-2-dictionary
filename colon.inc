%macro colon 2
    %ifdef ELEMENT
        %2: dq ELEMENT
    %else
        %2: dq 0
    %endif
    db %1, 0
    %define ELEMENT %2
%endmacro